---
layout: handbook-page-toc
title: "Be clear about sticky bears"
description: "As a leader it's important to remember that what you say leads people to take action."
---

## Be clear

As a leader it's important to remember that what you say leads people to take action. If you voice a vague frustration it might send people on your team looking for a solution you don't need. 

Here's an extreme example of what not to do from Gavin Belson, CEO of [Hooli](https://hooli.xyz).

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/i92Ws7qPTRg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>